const postService = require("../../../services/postService");

module.exports = {
  register(req, res) {
    postService
      .register(req.body)
      .then((data) => {
        res.status(201).json({
          status: "Created",
          data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  login(req, res) {
    postService
      .login(req.body)
      .then((data) => {
        res.status(200).json({
          status: "Login Success",
          data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  addAdmin(req, res) {
    postService
      .addAdmin(req.body)
      .then((data) => {
        res.status(201).json({
          status: "Created",
          data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },

  addAdminFromMember(req, res) {
    postService
      .addAdminFromMember(req.params.id, req.body)
      .then((data) => {
        res.status(200).json({
          status: "Admin Added",
          data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
  create(req, res) {
    postService
      .create(req.body)
      .then((data) => {
        res.status(201).json({
          status: "Created",
          data,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
  list(req, res) {
    postService
      .list()
      .then(({ data, count }) => {
        res.status(200).json({
          status: "OK",
           data ,
          "total data":count ,
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
  listById(req, res) {
    postService
      .listById(req.params.id)
      .then((data) => {
        res.status(200).json({
          status: "OK",
          data : { data },
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
  update(req, res) {
    postService
      .update(req.params.id, req.body)
      .then((data) => {
        res.status(200).json({
          status: "Data Updated"
        });
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
  delete(req, res) {
    postService
      .delete(req.params.id)
      .then(() => {
        res.status(200).json({
          status: "Data Deleted",
        }).end();
      })
      .catch((err) => {
        res.status(400).json({
          status: "FAIL",
          message: err.message,
        });
      });
  },
  
};
