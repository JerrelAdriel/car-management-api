const postRepository = require("../repositories/postRepository");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

module.exports = {
  async register(data) {
    data.password = await this.encryptPassword(data.password);
    data.role = "member";
    return postRepository.register(data);
  },

  async login(data) {
    data.email = data.email.toLowerCase();

    const user = await postRepository.findByEmail(data.email);

    if (!user) {
      throw new Error('Email tidak ditemukan');
    }

    if (user.role != "superadmin") {
      const isPasswordCorrect = await this.checkPassword(
        user.password,
        data.password
      );
  
      if (!isPasswordCorrect) {
        throw new Error('Password salah');
      }
      
    }
    const token = await this.createToken({
      email: data.email,
      password: data.password 
    });

    return {
      email: data.email,
      token
    };
  },

  async encryptPassword(password) {
    return bcrypt.hashSync(password, 10);
  },

  async checkPassword(encryptedPassword, bodyPassword) {
    return bcrypt.compare(
      bodyPassword,
      encryptedPassword,
    );
  },

  async createToken(data) {
    return jwt.sign(data, process.env.JWT_SECRET);
  },

  async addAdmin(data) {
    data.password = await this.encryptPassword(data.password);
    data.role = "admin";
    return postRepository.register(data);
  },

  async addAdminFromMember(id, data) {
    return postRepository.addAdminFromMember(id, data);
  },
  
  async create(data) {
    return postRepository.create(data);
  },

  async update(id, data) {
    return postRepository.update(id, data);
  },

  async list() {
    try {
      const posts = await postRepository.list();
      const postCount = await postRepository.getTotalPost();
      return {
        data: posts,
        count: postCount, 
      };
    } catch (err) {
      throw err;
    }
  },

  async listById(id) {
    try {
      const post = await postRepository.listById(id);
      return post;
    } catch (err) {
      throw err;
    }
  },

  async delete(id) {
    try {
      const post = await postRepository.delete(id);
      return post;
    } catch (err) {
      throw err;
    }
  }

};
