const { User, Car } = require("../models");

module.exports = {
  register(data) {
    return User.create(data);
  },
  
  addAdminFromMember(id, data) {
    return User.update(data, {
      where: { id }
    });
  },

  findByEmail(email) {
    return User.findOne({
      where: { email }
    });
  },

  create(data) {
    return Car.create(data);
  },

  list(){
    return Car.findAll();
  },

  listById(id){
    return Car.findOne({
      where: { id }
    });
  },

  getTotalPost() {
    return Car.count();
  },

  update(id, data) {
    return Car.update(data, {
      where: { id }
    });
  },
  
  delete(id) {
    return Car.destroy({
      where: { id }
    });
  },


};
