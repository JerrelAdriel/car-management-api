const express = require("express");
const controllers = require("../app/controllers");
const authorization = require("../app/middlewares/authorization")
const YAML = require("yamljs");
const swaggerUi = require("swagger-ui-express");

const yamlDocument = YAML.load("./documentationAPI.yaml");

const apiRouter = express.Router();

/**
 * TODO: Implement your own API
 *       implementations
 */
apiRouter.post("/api/v1/register", controllers.api.v1.postController.register);
apiRouter.post("/api/v1/login", controllers.api.v1.postController.login);

apiRouter.post("/api/v1/addAdmin", authorization.checkTokenSuperAdmin, controllers.api.v1.postController.addAdmin);
apiRouter.post("/api/v1/addAdminFromMember/:id", authorization.checkTokenSuperAdmin, controllers.api.v1.postController.addAdminFromMember);
apiRouter.post("/api/v1/create-car", authorization.checkTokenNotMember, controllers.api.v1.postController.create);
apiRouter.get("/api/v1/list-car", authorization.checkToken, controllers.api.v1.postController.list);
apiRouter.get("/api/v1/list-carById/:id", authorization.checkTokenNotMember, controllers.api.v1.postController.listById);
apiRouter.post("/api/v1/update-car/:id", authorization.checkTokenNotMember, controllers.api.v1.postController.update);
apiRouter.get("/api/v1/delete-car/:id", authorization.checkTokenNotMember, controllers.api.v1.postController.delete);

apiRouter.use("/api/v1/api-docs", swaggerUi.serve, swaggerUi.setup(yamlDocument));

/**
 * TODO: Delete this, this is just a demonstration of
 *       error handler
 */

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
